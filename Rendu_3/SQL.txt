CREATE TYPE typePersonne AS ENUM ('Client', 'Veterinaire', 'Assistant');
CREATE TABLE Personne
(
    id SERIAL NOT NULL PRIMARY KEY, 
    nom VARCHAR, 
    prenom VARCHAR, 
    dateNaissance DATE, 
    adresse VARCHAR,
    numtel VARCHAR, 
    type typePersonne NOT NULL
);

CREATE TYPE catEspece AS ENUM ('Felins', 'Canides', 'Reptiles', 'Rongeurs', 'Oiseaux', 'Autres');
CREATE TABLE Espece
(
    nom VARCHAR NOT NULL PRIMARY KEY,
    categorie catEspece
);

CREATE TABLE Animal
(
    numPuceId INTEGER NOT NULL PRIMARY KEY,
    nom VARCHAR,
    dateNaissance DATE,
    numPasseport INTEGER,
    espece VARCHAR NOT NULL,
    FOREIGN KEY (espece) REFERENCES Espece(nom)
);

CREATE TABLE Specialisation
(
    idPersonne SERIAL NOT NULL,
    espece VARCHAR NOT NULL,
    PRIMARY KEY(idPersonne, espece),
    FOREIGN KEY(idPersonne) REFERENCES Personne(id),
    FOREIGN KEY(espece) REFERENCES Espece(nom)
);

CREATE TABLE Proprietaire
(
    client SERIAL NOT NULL,
    animal INTEGER NOT NULL,
    dateDebutDetention DATE,
    dateFinDetetion DATE CHECK (dateFinDetetion > dateDebutDetention),
    PRIMARY KEY(client, animal),
    FOREIGN KEY (client) REFERENCES Personne(id),
    FOREIGN KEY (animal) REFERENCES Animal(numPuceId)
);

CREATE TABLE SuiviVeterinaire
(
    veterinaire SERIAL NOT NULL,
    animal INTEGER NOT NULL,
    dateDebutSuivi DATE,
    dateFinSuivi DATE CHECK (dateFinSuivi > dateDebutSuivi),
    PRIMARY KEY(veterinaire, animal),
    FOREIGN KEY (veterinaire) REFERENCES Personne(id),
    FOREIGN KEY (animal) REFERENCES Animal(numPuceId)
);

CREATE TABLE Consultation
(
    id SERIAL NOT NULL PRIMARY KEY,
    time TIMESTAMP,
    animal INTEGER NOT NULL,
    personnel SERIAL NOT NULL,
    FOREIGN KEY (animal) REFERENCES Animal(numPuceId),
    FOREIGN KEY (personnel) REFERENCES Personne(id)
);


CREATE TYPE uniteMesure AS ENUM ('Grammes', 'Centimetres');
CREATE TABLE Mesure
(
    id INTEGER NOT NULL PRIMARY KEY,
    valeur INTEGER,
    unites uniteMesure,
    saisie TIMESTAMP,
    consultation SERIAL NOT NULL,
    FOREIGN KEY (consultation) REFERENCES Consultation(id)
);

CREATE TABLE Resultat
(
    id INTEGER NOT NULL PRIMARY KEY,
    lien VARCHAR,
    saisie TIMESTAMP,
    consultation SERIAL NOT NULL,
    FOREIGN KEY (consultation) REFERENCES Consultation(id)
);

CREATE TABLE Observation
(
    id INTEGER NOT NULL PRIMARY KEY,
    observation VARCHAR,
    saisie TIMESTAMP,
    consultation SERIAL NOT NULL,
    FOREIGN KEY (consultation) REFERENCES Consultation(id)
);

CREATE TABLE Traitement
(
    id INTEGER NOT NULL PRIMARY KEY,
    dateDebut DATE,
    duree VARCHAR,
    veterinaireQuiPrescrit SERIAL NOT NULL,
    saisie TIMESTAMP,
    consultation SERIAL NOT NULL,
    FOREIGN KEY (consultation) REFERENCES Consultation(id),
    FOREIGN KEY (veterinaireQuiPrescrit) REFERENCES Personne(id)
);

CREATE TABLE Procedure
(
    id INTEGER NOT NULL PRIMARY KEY,
    nom VARCHAR,
    description VARCHAR,
    saisie TIMESTAMP,
    consultation SERIAL NOT NULL,
    FOREIGN KEY (consultation) REFERENCES Consultation(id)
);

CREATE TABLE Medicament
(
    nomMolecule VARCHAR NOT NULL PRIMARY KEY,
    descriptionEffets VARCHAR
);

CREATE TABLE AutorisationMedicament
(
    espece VARCHAR NOT NULL,
    medicament VARCHAR NOT NULL,
    PRIMARY KEY(espece, medicament),
    FOREIGN KEY (espece) REFERENCES Espece(nom),
    FOREIGN KEY (medicament) REFERENCES Medicament(nomMolecule)
);

CREATE TABLE Prescription
(
    traitement INTEGER NOT NULL,
    medicament VARCHAR NOT NULL,
    quantite VARCHAR,
    PRIMARY KEY(traitement, medicament),
    FOREIGN KEY (traitement) REFERENCES Traitement(id),
    FOREIGN KEY (medicament) REFERENCES Medicament(nomMolecule)
);



INSERT INTO Personne VALUES (0, 'Bridoux', 'Thomas', '2000-10-27', '14 rue d''Amiens, Compiègne', '0672013968', 'Client');
INSERT INTO Personne VALUES (1, 'Marchand', 'Marie', '1987-03-24', '20 rue de l''église, Amiens', '0614963968', 'Veterinaire');
INSERT INTO Personne VALUES (2, 'Gricourt', 'Celia', '2001-05-15', '14 rue du General de Gaulle, Coulomniers', '0614961485', 'Assistant');

INSERT INTO Espece VALUES ('Canis lupus familiaris', 'Canides');
INSERT INTO Espece VALUES ('Felis catus', 'Felins');
INSERT INTO Espece VALUES ('Mustela putorius furo', 'Rongeurs');

INSERT INTO Specialisation VALUES (1, 'Canis lupus familiaris');
INSERT INTO Specialisation VALUES (2, 'Felis catus');

INSERT INTO Animal VALUES (1234, 'Pupuce', '2018-10-12', '1524898', 'Felis catus');
INSERT INTO Animal VALUES (4759, 'Medor', '2015-10-12', '1147884', 'Canis lupus familiaris');
INSERT INTO Animal VALUES (4758, 'Melou', '2017-10-12', '4874782', 'Canis lupus familiaris');

INSERT INTO Proprietaire VALUES (0, 1234, '2020-10-25', NULL);
INSERT INTO Proprietaire VALUES (0, 4759, '2020-10-25', NULL);
INSERT INTO Proprietaire VALUES (0, 4758, '2020-10-25', NULL);

INSERT INTO SuiviVeterinaire VALUES (2, 1234, '2020-03-12', NULL);
INSERT INTO SuiviVeterinaire VALUES (2, 4759, '2020-03-12', NULL);
INSERT INTO SuiviVeterinaire VALUES (2, 4758, '2020-03-12', NULL);

INSERT INTO Consultation VALUES (0, '2022-10-30 15:00:00', 1234, 1);
INSERT INTO Consultation VALUES (1, '2022-11-12 14:00:00', 4758, 1);

INSERT INTO Mesure VALUES(0, 183, 'Centimetres', '2022-10-30 15:22:00', 0);
INSERT INTO Resultat VALUES(1, 'http://resultat-analyse.fr/frezf', '2022-10-30 15:34:00', 0);
INSERT INTO Observation VALUES(2, 'Fracture de la patte arrière gauche', '2022-10-30 15:34:00', 0);
INSERT INTO Procedure VALUES(3,  'Incision', 'Incision au niveau de la patte arrière gauche', '2022-10-30 15:40:00', 0);
INSERT INTO Traitement VALUES(4, '2022-10-30', '3 jours', 1, '2022-10-30 15:50:00', 0);

INSERT INTO Medicament VALUES('Fluoxetine', 'Antibiotique');

INSERT INTO AutorisationMedicament VALUES('Felis catus', 'Fluoxetine');


INSERT INTO Prescription VALUES(4, 'Fluoxetine', '100 mg');