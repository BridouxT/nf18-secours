# NF18_projet

## Membres du groupe

Tianchenghao

raphael_chauvier

Thomas Bridoux

sore

## Sujet

Veto

Gestion d'une clinique vétérinaire
On vous charge de réaliser une application de gestion pour une clinique vétérinaire. L'administrateur de la clinique souhaite pouvoir gérer ses patients (animaux), ses clients, son personnel soignant ainsi que les médicaments administrés. Les clients et les personnels ont tous des noms, prénoms, date de naissance, une adresse et un numéro de téléphone. Les personnels ont en plus un poste (vétérinaire ou assistant) et une spécialité : les espèces animales qu'ils savent le mieux traiter. La clinique se spécialise dans le traitement d'animaux domestiques de petites et moyennes tailles, ils peuvent être des félins, des canidés, des reptiles, des rongeurs, ou des oiseaux. Exceptionnellement, elle peut traiter des animaux qui ne figurent pas parmi ces catégories et les regroupe dans une classe « autres ».

Pour chaque animal traité, la clinique souhaite garder son nom, son espèce, sa date de naissance (qui peut être juste une année, ou inconnue), le numéro de sa puce d'identification (s'il en a), son numéro de passeport (s'il en a), la liste de ses propriétaires et la période durant laquelle l'animal était avec eux, ainsi que la liste des vétérinaires qui l'ont suivi et quand est-ce qu'ils l'ont fait. Il faut noter que le personnel de la clinique ne doit pas avoir d'animaux de compagnie traités dans la clinique.

La clinique souhaite aussi garder le dossier médical de ses patients. Un dossier médical contient plusieurs entrés de différents types :

Une mesure de sa taille ou de son poids.

Un traitement prescrit avec la date de début, la durée, le nom et la quantité à prendre par jour pour chaque médicament prescrit (on peut prescrire plusieurs molécules dans un traitement). Seul un vétérinaire peut prescrire un traitement.

Des résultats d'analyses (sous forme de lien vers un document électronique)

Une observation générale faite lors d'une consultation et qui l'a faite.

Une procédure réalisée sur le patient avec sa description.

Pour chaque entrée, on veut garder la date et l'heure auxquelles elle a été saisie.

Enfin, les médicaments sont identifiés par le nom de la molécule et sont accompagnés de quelques lignes de texte décrivant leurs effets. Un médicament n'est autorisé que pour certaines espèces.

Besoins
Le gestionnaire de la clinique veut pouvoir ajouter et mettre à jour la liste des personnels, des clients, des animaux traités et les médicaments utilisés. Il doit aussi pouvoir obtenir facilement des rapports d'activité et des informations statistiques, comme les quantités de médicaments consommés, le nombre de traitement ou de procédure effectuées dans la clinique, ou encore des statistiques sur les espèces d'animaux traités.

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.utc.fr/tianchen/nf18_projet.git
git branch -M main
git push -uf origin main
```

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.
